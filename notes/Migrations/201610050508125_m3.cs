namespace notes.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class m3 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Noteadditions", "Message", c => c.String(nullable: false));
            AlterColumn("dbo.Noteadditions", "Code", c => c.String(nullable: false));
            AlterColumn("dbo.Noteadditions", "CodeName", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Noteadditions", "CodeName", c => c.String());
            AlterColumn("dbo.Noteadditions", "Code", c => c.String());
            AlterColumn("dbo.Noteadditions", "Message", c => c.String());
        }
    }
}
