namespace notes.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _1m : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Noteadditions", "dateadd", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Noteadditions", "dateadd");
        }
    }
}
