namespace notes.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class mig900 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Notemessages", "Noteaddition_Id", "dbo.Noteadditions");
            DropForeignKey("dbo.Noteadditions", "Notemessage_Id", "dbo.Notemessages");
            DropIndex("dbo.Notemessages", new[] { "Noteaddition_Id" });
            DropIndex("dbo.Noteadditions", new[] { "Notemessage_Id" });
            DropColumn("dbo.Notemessages", "Noteaddition_Id");
            DropColumn("dbo.Noteadditions", "Notemessage_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Noteadditions", "Notemessage_Id", c => c.Int());
            AddColumn("dbo.Notemessages", "Noteaddition_Id", c => c.Int());
            CreateIndex("dbo.Noteadditions", "Notemessage_Id");
            CreateIndex("dbo.Notemessages", "Noteaddition_Id");
            AddForeignKey("dbo.Noteadditions", "Notemessage_Id", "dbo.Notemessages", "Id");
            AddForeignKey("dbo.Notemessages", "Noteaddition_Id", "dbo.Noteadditions", "Id");
        }
    }
}
