namespace notes.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class mig78686 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Noteadditions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Message = c.String(),
                        Code = c.String(),
                        CodeName = c.String(),
                        NotemessageId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Notemessages", t => t.NotemessageId, cascadeDelete: true)
                .Index(t => t.NotemessageId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Noteadditions", "NotemessageId", "dbo.Notemessages");
            DropIndex("dbo.Noteadditions", new[] { "NotemessageId" });
            DropTable("dbo.Noteadditions");
        }
    }
}
