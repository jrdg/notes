namespace notes.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _500 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Noteadditions", "NotemessageId", "dbo.Notemessages");
            AddColumn("dbo.Noteadditions", "Notemessage_Id", c => c.Int());
            AddColumn("dbo.Noteadditions", "Notemessage_Id1", c => c.Int());
            CreateIndex("dbo.Noteadditions", "Notemessage_Id");
            CreateIndex("dbo.Noteadditions", "Notemessage_Id1");
            AddForeignKey("dbo.Noteadditions", "Notemessage_Id1", "dbo.Notemessages", "Id");
            AddForeignKey("dbo.Noteadditions", "Notemessage_Id", "dbo.Notemessages", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Noteadditions", "Notemessage_Id", "dbo.Notemessages");
            DropForeignKey("dbo.Noteadditions", "Notemessage_Id1", "dbo.Notemessages");
            DropIndex("dbo.Noteadditions", new[] { "Notemessage_Id1" });
            DropIndex("dbo.Noteadditions", new[] { "Notemessage_Id" });
            DropColumn("dbo.Noteadditions", "Notemessage_Id1");
            DropColumn("dbo.Noteadditions", "Notemessage_Id");
            AddForeignKey("dbo.Noteadditions", "NotemessageId", "dbo.Notemessages", "Id", cascadeDelete: true);
        }
    }
}
