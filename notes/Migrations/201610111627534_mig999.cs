namespace notes.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class mig999 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "Actif", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "Actif");
        }
    }
}
