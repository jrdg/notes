namespace notes.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class uyftyduyiygut : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Subscribes", "NotemessageId");
            AddForeignKey("dbo.Subscribes", "NotemessageId", "dbo.Notemessages", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Subscribes", "NotemessageId", "dbo.Notemessages");
            DropIndex("dbo.Subscribes", new[] { "NotemessageId" });
        }
    }
}
