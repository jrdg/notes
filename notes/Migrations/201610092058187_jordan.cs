namespace notes.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class jordan : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Noteadditions", "Code");
            DropColumn("dbo.Noteadditions", "CodeName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Noteadditions", "CodeName", c => c.String(nullable: false));
            AddColumn("dbo.Noteadditions", "Code", c => c.String(nullable: false));
        }
    }
}
