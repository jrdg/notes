namespace notes.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class mig100000 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Notemessages", "Public", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Notemessages", "Public");
        }
    }
}
