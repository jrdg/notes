namespace notes.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _700 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Noteadditions", "Notemessage_Id1", "dbo.Notemessages");
            DropIndex("dbo.Noteadditions", new[] { "Notemessage_Id1" });
            AddColumn("dbo.Notemessages", "Noteaddition_Id", c => c.Int());
            CreateIndex("dbo.Notemessages", "Noteaddition_Id");
            AddForeignKey("dbo.Notemessages", "Noteaddition_Id", "dbo.Noteadditions", "Id");
            DropColumn("dbo.Noteadditions", "Notemessage_Id1");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Noteadditions", "Notemessage_Id1", c => c.Int());
            DropForeignKey("dbo.Notemessages", "Noteaddition_Id", "dbo.Noteadditions");
            DropIndex("dbo.Notemessages", new[] { "Noteaddition_Id" });
            DropColumn("dbo.Notemessages", "Noteaddition_Id");
            CreateIndex("dbo.Noteadditions", "Notemessage_Id1");
            AddForeignKey("dbo.Noteadditions", "Notemessage_Id1", "dbo.Notemessages", "Id");
        }
    }
}
