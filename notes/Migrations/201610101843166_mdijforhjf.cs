namespace notes.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class mdijforhjf : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "AvatarId", c => c.Int());
            CreateIndex("dbo.Users", "AvatarId");
            AddForeignKey("dbo.Users", "AvatarId", "dbo.Avatars", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Users", "AvatarId", "dbo.Avatars");
            DropIndex("dbo.Users", new[] { "AvatarId" });
            DropColumn("dbo.Users", "AvatarId");
        }
    }
}
