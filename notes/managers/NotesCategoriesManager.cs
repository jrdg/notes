﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using notes.Models;
using System.Web.Mvc;

namespace notes.managers
{
    public static class NotesCategoriesManager
    {
        public static int getId(String name)
        {
            Categorie cat = null;

            using (Database ctx = new Database())
            {
                cat = ctx.Categories.Where(c => c.Name == name).FirstOrDefault();
            }

            return cat.Id;
        }

        public static List<Categorie> getAll()
        {
            List<Categorie> categories = null;

            using (Database ctx = new Database())
            {
                categories = ctx.Categories.Include("Notetopic").ToList();
            }

            return categories;
        }

        public static bool CategorieNameExist(String name)
        {
            using (Database ctx = new Database())
            {
                Categorie c = ctx.Categories.Where(n => n.Name == name).FirstOrDefault();

                if (c != null)
                    return true;
                else
                    return false;
            }
        }
    }
}