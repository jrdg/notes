﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using notes.Models;
using System.Security.Cryptography;
using System.Text;
using System.Data.Entity.Validation;
using System.Diagnostics;

namespace notes.managers
{
    public static class UsersManager
    {
        public static void Banned(String username , String banOrDeban)
        {
            using (Database ctx = new Database())
            {
                User user = ctx.Users.Where(u => u.Username == username).FirstOrDefault();

                if(user != null)
                {
                    if (banOrDeban == "Ban")
                        user.Actif = false;
                    else
                        user.Actif = true;

                    ctx.Configuration.ValidateOnSaveEnabled = false;
                    ctx.SaveChanges();
                }
            }
        }

        public static void Edit(SettingFormModel user)
        {
            using (Database ctx = new Database())
            {
                User usr = ctx.Users.Where(u => u.Id == user.Id).FirstOrDefault();

                if(usr != null)
                {
                    usr.Email = user.Email;

                    if (user.Password != null)
                        usr.Password = user.Password;

                    if (user.IdAvatar != null)
                        usr.AvatarId = user.IdAvatar;

                    ctx.Configuration.ValidateOnSaveEnabled = false;
                    ctx.SaveChanges();
                }
            }
        }

        public static void Add(User user)
        {
            using (Database ctx = new Database())
            {
                ctx.Users.Add(user);
                ctx.SaveChanges();
            }
        }

        public static bool ValidBirthDay(User user)
        {
            if (user.Day < 1 || user.Day > 31)
                return false;
            if (user.Month < 1 || user.Month > 12)
                return false;
            if (user.Year < 1900 || user.Year > DateTime.Now.Year)
                return false;
            return true;
        }

        public static String MD5Hash(String str)
        {
            UTF8Encoding utf8 = new UTF8Encoding();
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            str = BitConverter.ToString(md5.ComputeHash(utf8.GetBytes(str)));
            return str;
        }

        public static bool EmailTaken(String email)
        {
            using (Database ctx = new Database())
            {
                User user = ctx.Users.Where(u => u.Email.ToLower() == email.ToLower()).FirstOrDefault();

                if (user != null)
                    return true;
                else
                    return false;
            }
        }

        public static bool UsernameTaken(String username)
        {
            using (Database ctx = new Database())
            {
                User user = ctx.Users.Where(u => u.Username.ToLower() == username.ToLower()).FirstOrDefault();

                if (user != null)
                    return true;
                else
                    return false;
            }
        }

        public static User getUserByUsername(String username)
        {
            using (Database ctx = new Database())
            {
                User user = ctx.Users.Include("Avatar").Include("Role").Where(u => u.Username == username).FirstOrDefault();

                if (user != null)
                    return user;
                else
                    return null;
            }
        }

        public static List<User> getUsersContain(String search)
        {
            using (Database ctx = new Database())
            {
                if (search == "")
                    return null;
                    
                return ctx.Users.Include("Avatar").ToList().FindAll(n => n.Username.ToLower().Contains(search.ToLower()));
            }
        }
    }
}