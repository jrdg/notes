﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using notes.Models;

namespace notes.managers
{
    public static class DropDownListManager
    {
        public static IEnumerable<SelectListItem> getBanDeban()
        {
            List<DropdownModel> dmlist = new List<DropdownModel>
           {
               new DropdownModel { Text = "Ban" , Value = "Ban" },
               new DropdownModel { Text = "Deban" , Value = "Deban" }
           };

           IEnumerable<SelectListItem> dmselect = dmlist.Select(d => new SelectListItem
           {
               Text = d.Text,
               Value = d.Value
           });

           return dmselect;
        }

        public static IEnumerable<SelectListItem> getCodeNameSelect(String cat)
        {
            List<DropdownModel> daylist = new List<DropdownModel>();

            daylist.Add(new DropdownModel { Text = "Java", Value = "java" });
            daylist.Add(new DropdownModel { Text = "Php", Value = "php" });
            daylist.Add(new DropdownModel { Text = "C#", Value = "csharp" });
            daylist.Add(new DropdownModel { Text = "Html", Value = "html" });
            daylist.Add(new DropdownModel { Text = "Css", Value = "css" });
            daylist.Add(new DropdownModel { Text = "Python", Value = "python" });
            daylist.Add(new DropdownModel { Text = "C", Value = "c" });

            IEnumerable<SelectListItem> dayselect = daylist.Select(d => new SelectListItem
            {
                Selected = (d.Text == cat),
                Text = d.Text,
                Value = d.Value.ToString()
            });

            return dayselect;
        }

        public static IEnumerable<SelectListItem> getNotetopicSelectListItem(String categorieName)
        {
            List<Categorie> nlist = NotesCategoriesManager.getAll();

            IEnumerable<SelectListItem> nselect = nlist.ToList().Select(n => new SelectListItem
            {
                Selected = (n.Name == categorieName),
                Text = n.Name,
                Value = n.Id.ToString()
            });

            return nselect;
        }

        public static IEnumerable<SelectListItem> getDays()
        {
            List<Day> daylist = new List<Day>();

            daylist.Add(new Day { Value = -1, Text = "Day" });

            for(int i = 1; i <= 31; i++)
                daylist.Add(new Day { Value = i, Text = i.ToString() });

            IEnumerable<SelectListItem> dayselect = daylist.Select(d => new SelectListItem
            {
                Selected = (d.Text == "Day"),
                Text = d.Text,
                Value = d.Value.ToString()
            });

            return dayselect;
        }

        public static IEnumerable<SelectListItem> getMonths()
        {

            List<Month> monthslist = new List<Month>
            {
                new Month { Value = -1 , Text = "Month" },
                new Month { Value = 1 , Text = "January" },
                new Month { Value = 2 , Text = "Feburary" },
                new Month { Value = 3 , Text = "March" },
                new Month { Value = 4 , Text = "April" },
                new Month { Value = 5 , Text = "May" },
                new Month { Value = 6 , Text = "June" },
                new Month { Value = 7 , Text = "July" },
                new Month { Value = 8 , Text = "August" },
                new Month { Value = 9 , Text = "September" },
                new Month { Value = 10 , Text = "October" },
                new Month { Value = 11 , Text = "November" },
                new Month { Value = 12 , Text = "December" },
            };

            IEnumerable<SelectListItem> monthselect = monthslist.Select(d => new SelectListItem
            {
                Selected = (d.Text == "Month"),
                Text = d.Text,
                Value = d.Value.ToString()
            });

            return monthselect;
        }

        public static IEnumerable<SelectListItem> getYears()
        {
            List<Years> yearslist = new List<Years> { new Years { Value = -1, Text = "Year" } };

            for(int i = 1900; i <= DateTime.Now.Year; i++)
                yearslist.Add(new Years { Value = i, Text = i.ToString() });

            IEnumerable<SelectListItem> yearselect = yearslist.Select(d => new SelectListItem
            {
                Selected = (d.Text == "Year"),
                Text = d.Text,
                Value = d.Value.ToString()
            });

            return yearselect;
        }
    }
}