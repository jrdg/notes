﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using notes.Models;

namespace notes.managers
{
    public static class AvatarManager
    {
        public static int Add(Avatar avatar)
        {
            using (Database ctx = new Database())
            {
                Avatar a = ctx.Avatars.Add(avatar);
                ctx.SaveChanges();
                return a.Id;
            }
        }

        public static bool doUserHaveAvatar(int? id)
        {
            using (Database ctx = new Database())
            {
                User user = ctx.Users.Where(u => u.Id == id).FirstOrDefault();

                if(user != null)
                {
                    if (user.AvatarId != null)
                        return true;
                }

                return false;
            }
        }

        public static void Delete(int? id)
        {
            using (Database ctx = new Database())
            {
                Avatar avatar = ctx.Avatars.Where(a => a.Id == id).FirstOrDefault();

                if(avatar != null)
                {
                    ctx.Avatars.Remove(avatar);
                    ctx.SaveChanges();
                }
            }
        }

        public static Avatar getById(int? id)
        {
            using (Database ctx = new Database())
            {
                return ctx.Avatars.Where(a => a.Id == id).FirstOrDefault();
            }
        }
    }
}