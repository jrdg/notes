﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace notes.managers
{
    public class FilesManager
    {
        public static char DirSeparator = System.IO.Path.DirectorySeparatorChar;
        public static String FilesPath = HttpContext.Current.Server.MapPath("~\\Content" + DirSeparator + "hhhhhhhhhhhhhhhh" + DirSeparator);

        public static String UploadFile(HttpPostedFileBase file , String userId)
        {
            // Check if the directory we are saving to exists
            if (!Directory.Exists(FilesPath))
            {
                // If it doesn't exist, create the directory
                Directory.CreateDirectory(FilesPath);
            }

            if(doExist("avatar-"+userId+".png"))
                Delete("avatar-" + userId + ".png");     

            // Set our full path for savings
            String path = FilesPath + DirSeparator + file.FileName;

            // Save our file
           file.SaveAs(Path.GetFullPath(path));

            Rename(file.FileName, "avatar-" + userId+".png");

            return "avatar-" + userId + ".png";
        }

        //regarde si le fichier existe deja
        public static bool doExist(String name)
        {
            String path = FilesPath + DirSeparator + name;
            return File.Exists(Path.GetFullPath(path));
        }

        public static void Delete(String name)
        {
            String path = FilesPath + DirSeparator + name;
            File.Delete(Path.GetFullPath(path));
        }

        public static void Rename(String oldfile , String newfile)
        {
            String nf = FilesPath + DirSeparator + newfile;
            String of = FilesPath + DirSeparator + oldfile;

            File.Copy(Path.GetFullPath(of) , Path.GetFullPath(nf));
            File.Delete(Path.GetFullPath(of));
        }
    }
}