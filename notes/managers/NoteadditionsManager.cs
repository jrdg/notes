﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using notes.Models;

namespace notes.managers
{
    public static class NoteadditionsManager
    {
        public static void Edit(Noteaddition addition)
        {
            using (Database ctx = new Database())
            {
                Noteaddition add = ctx.Noteadditions.Where(n => n.Id == addition.Id).FirstOrDefault();
                
                if (add != null)
                {
                    add.Message = addition.Message;
                    ctx.SaveChanges();
                }
            }
        }

        public static Noteaddition get(int? id)
        {
            Noteaddition note = null;

            using (Database ctx = new Database())
            {
                note = ctx.Noteadditions.Include("Notemessage").Include("Notemessage.Categorie").Where(n => n.Id == id).FirstOrDefault();
            }

            return note;
        }

        public static void Add(Noteaddition noteaddition)
        {
            using (Database ctx = new Database())
            {
                noteaddition.Dateadd = DateTime.Now;
                ctx.Noteadditions.Add(noteaddition);
                ctx.SaveChanges();
            }
        }

        public static void Delete(int? additionID)
        {
            using (Database ctx = new Database())
            {
                Noteaddition todelete = ctx.Noteadditions.Where(n => n.Id == additionID).FirstOrDefault();

                if (todelete != null)
                {
                    ctx.Noteadditions.Remove(todelete);
                    ctx.SaveChanges();
                }
            }
        }
    }
}