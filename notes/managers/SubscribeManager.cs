﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using notes.Models;

namespace notes.managers
{
    public static class SubscribeManager
    {
        public static List<Notemessage> getUserId(int? id)
        {
            using (Database ctx = new Database())
            {
                List<int> messagesId = ctx.Subscribes.Where(su => su.UserId == id).Select(s => s.NotemessageId).ToList();
                return ctx.Notemessage.Include("Noteadditions").Include("Categorie").Include("User").OrderByDescending(g => g.Noteadditions.Max(l => l.Dateadd)).Where(k => messagesId.Contains(k.Id)).ToList();
            }
        }

        public static void Delete(Subscribe sub)
        {
            using (Database ctx = new Database())
            {
                Subscribe subscribe = ctx.Subscribes.Where(s => s.NotemessageId == sub.NotemessageId && s.UserId == sub.UserId).FirstOrDefault();

                if(subscribe != null)
                {
                    ctx.Subscribes.Remove(subscribe);
                    ctx.SaveChanges();
                }
            }
        }

        public static void Add(Subscribe sub)
        {
            using (Database ctx = new Database())
            {
                ctx.Subscribes.Add(sub);
                ctx.SaveChanges();
            }
        }

        public static bool doExist(Subscribe sub)
        {
            using (Database ctx = new Database())
            {
                Subscribe subscribe = ctx.Subscribes.Where(s => s.UserId == sub.UserId && s.NotemessageId == sub.NotemessageId).FirstOrDefault();

                if (subscribe != null)
                    return true;
                else
                    return false;
            }
        }
    }
}