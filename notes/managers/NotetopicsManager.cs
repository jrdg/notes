﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using notes.Models;
using System.Data.Entity.Validation;
using System.Diagnostics;

namespace notes.managers
{
    public static class NotetopicsManager
    {
        public static List<Notemessage> AllMessageFromUser(int id , String search)
        {
            using (Database ctx = new Database())
            {
                return ctx.Notemessage.Include("Categorie").Include("User").ToList().FindAll(n => n.Title.ToLower().Contains(search.ToLower()) && n.UserId == id);
            }
        }

        public static void Delete(int? id)
        {
            using (Database ctx = new Database())
            {
                Notemessage msg = ctx.Notemessage.Where(n => n.Id == id).FirstOrDefault();

                if(msg != null)
                {
                    ctx.Notemessage.Remove(msg);
                    ctx.SaveChanges();
                }
            }
        }

        public static void edit(Notemessage model)
        {
            using (Database ctx = new Database())
            {

                Notemessage msg = ctx.Notemessage.Where(n => n.Id == model.Id).FirstOrDefault();
                System.Diagnostics.Debug.WriteLine("id to changeeeeeeeeeeeeeee : " + msg.Id);
                System.Diagnostics.Debug.WriteLine("titreeeeeeeeeeeeeeeee : "+model.Title);
                System.Diagnostics.Debug.WriteLine("categorieeeeeeeeeeeeeeeeeee : " +model.CategorieId );
                System.Diagnostics.Debug.WriteLine("descriptionnnnnnnnnnnnnnnnnnn : "+model.Description);

                if (msg != null)
                {
                    msg.Title = model.Title;
                    msg.CategorieId = model.CategorieId;
                    msg.Description = model.Description;
                    msg.Public = model.Public;
                }

                try
                {
                    ctx.SaveChanges();
                }
                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            Trace.TraceInformation("Property: {0} Error: {1}",
                                                    validationError.PropertyName,
                                                    validationError.ErrorMessage);
                        }
                    }
                }
            }
        }

        public static int Add(Notemessage model)
        {
            using (Database ctx = new Database())
            {
                ctx.Notemessage.Add(model);
                ctx.SaveChanges();
            }

            return model.Id;
        }

        public static Notemessage getById(int? id)
        {
            Notemessage nm = null;

            using (Database ctx = new Database())
            {
                nm = ctx.Notemessage.Include("Noteadditions").Include("Categorie").Include("User").Where(n => n.Id == id).FirstOrDefault();
            }

            return nm;
        }

        public static List<Notemessage> getListMessageByUserIdAndCategorie(int? iduser , int? catid)
        {
            using (Database ctx = new Database())
            {
                return ctx.Notemessage.Include("Noteadditions").Include("Categorie").Include("User").OrderByDescending(n => n.Noteadditions.Max(l => l.Dateadd)).Where(nm => nm.UserId == iduser && nm.CategorieId == catid).ToList();
            }
        }

        public static List<Notemessage> getByUsernamePublic(String username)
        {
            using (Database ctx = new Database())
            {
                return ctx.Notemessage.Include("Noteadditions").Include("Categorie").OrderByDescending(g => g.Noteadditions.Max(l => l.Dateadd)).Where(n => n.user.Username == username && n.Public).ToList();
            }
        }

        public static List<Notemessage> getListContain(String search)
        {
            using (Database ctx = new Database())
            {
                if (search == "")
                    return null;

                return ctx.Notemessage.Include("Categorie").Include("User").ToList().FindAll(n => n.Title.ToLower().Contains(search.ToLower()) && n.Public);
            }
        }

        public static List<Notemessage> getLastNotes()
        {
            using (Database ctx = new Database())
            {
                return ctx.Notemessage.Include("Categorie").Include("User").OrderByDescending(n => n.Id).Take(20).Where(t => t.Public).ToList();
            }
        }

        public static List<Notemessage> getLastNotesWithRecentAddition()
        {
            using (Database ctx = new Database())
            {
                return ctx.Notemessage.Include("Noteadditions").Include("Categorie").Include("User").OrderByDescending(n => n.Noteadditions.Max(l => l.Dateadd)).Where(t => t.Public).Take(20).ToList();
            }
        }

    }
}