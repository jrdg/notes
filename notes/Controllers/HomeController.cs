﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using notes.Models;
using notes.managers;
using notes.formulaire;

namespace notes.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult admin()
        {
            User user = ((User)Session["userConnected"]);

            if(user == null)
                return RedirectToAction("Index");

            if(user.Role.Name != "Admin")
                return RedirectToAction("notfound");

            this.ViewBag.Options = DropDownListManager.getBanDeban();
            return View();
        }

        [HttpPost]
        public ActionResult admin(AdminModel model)
        {
            User user = UsersManager.getUserByUsername(model.Username);

            if (user == null)
                this.ModelState.AddModelError("Username", model.Username + " do not exist");
            else
            {
                if (model.BanOrDeban == "Ban")
                {
                    if (user.Actif == false)
                        this.ModelState.AddModelError("Username", model.Username + " is already banned");
                }
                else if(model.BanOrDeban == "Deban")
                {
                    if(user.Actif == true)
                        this.ModelState.AddModelError("Username", model.Username + " is not ban");
                }
            }



            if (this.ModelState.IsValid)
            {
                UsersManager.Banned(model.Username , model.BanOrDeban);
                model.Username = "";
                this.ModelState.Clear();
            }

            this.ViewBag.Options = DropDownListManager.getBanDeban();
            return View(model);
        }

        public ActionResult publicnote(int? id)
        {
            if (Session["userConnected"] == null)
                return RedirectToAction("Index");

            Notemessage message = NotetopicsManager.getById(id);

            if (message == null)
                return RedirectToAction("notfound");

            if (message.user.Id == ((User)Session["userConnected"]).Id)
                return RedirectToAction("note", new { categorie = message.Categorie.Name, id = id });
            else
            {
                if (!message.Public)
                    return RedirectToAction("notfound");
            }


            User user = ((User)Session["userConnected"]);

            if (SubscribeManager.doExist(new Subscribe { UserId = user.Id, NotemessageId = (int)id }))
                this.ViewData["subornot"] = true;
            else
                this.ViewData["subornot"] = false;

            return View(message);
        }

        public ActionResult subscribeadd(int? id)
        {
            if (Session["userConnected"] == null)
                return RedirectToAction("Index");

            User user = ((User)Session["userConnected"]);

            if(this.Request.IsAjaxRequest())
            {
                if (id != null)
                {
                    Subscribe sub = new Subscribe { UserId = user.Id, NotemessageId = (int)id };

                    if (!SubscribeManager.doExist(sub))
                    {
                        SubscribeManager.Add(sub);
                        return Json(new { value = true } , JsonRequestBehavior.AllowGet);
                    } 
                    else
                        return Json(new { value = false }, JsonRequestBehavior.AllowGet);
                }
            }

            return RedirectToAction("notfound");
        }

        public ActionResult subscribedel(int? id , String cate)
        {
            if (Session["userConnected"] == null)
                return RedirectToAction("Index");

            User user = ((User)Session["userConnected"]);

            if (this.Request.IsAjaxRequest())
            {
                if (id != null)
                {
                    Subscribe sub = new Subscribe { UserId = user.Id, NotemessageId = (int)id };

                    if (SubscribeManager.doExist(sub))
                    {
                        SubscribeManager.Delete(sub);
                        return Json(new { value = false }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { value = true }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                if (id != null)
                {
                    Subscribe sub = new Subscribe { UserId = user.Id, NotemessageId = (int)id };

                    if (SubscribeManager.doExist(sub))
                    {
                        SubscribeManager.Delete(sub);
                        //return Json(new { value = false }, JsonRequestBehavior.AllowGet);
                    }
                    // return Json(new { value = true }, JsonRequestBehavior.AllowGet);

                    return RedirectToAction("mynotes" , new { id = "Subscribes"});
                }
            }
                

                return RedirectToAction("notfound");
        }

        public ActionResult setting()
        {
            User user = ((User)Session["userConnected"]);

            if (user == null)
                return RedirectToAction("Index");

            if (user.Role.Name == "Admin")
                this.ViewBag.IsAdmin = true;
            else
                this.ViewBag.IsAdmin = false;

            return View(new SettingFormModel { Email = user.Email });
        }

        [HttpPost]
        public ActionResult setting(SettingFormModel model)
        {
            User userses = ((User)Session["userConnected"]);

            if (userses == null)
                return RedirectToAction("Index");

            String[] typeaccepted = { "image/png" };

            if(model.AvatarUrl != null)
            {
                if (!typeaccepted.Contains(model.AvatarUrl.ContentType))
                    this.ModelState.AddModelError("AvatarUrl", "Only png are allowed");
            }

            User userSession = ((User)Session["userConnected"]);

            if(model.Password != null)
            {
                if (model.Oldpassword != userSession.Password)
                    ModelState.AddModelError("Oldpassword", "Bad old password");
            }

            if (this.ModelState.IsValid)
            {
                model.Id = userSession.Id;

                int? oldAvatarId = null;

                if (model.AvatarUrl != null)
                {

                    String filename = FilesManager.UploadFile(model.AvatarUrl, userses.Id.ToString());
                    int avatar_id = AvatarManager.Add(new Avatar { Filename = filename, Path = FilesManager.FilesPath });
                    model.IdAvatar = avatar_id;

                    if(userses.Avatar != null)
                        oldAvatarId = userses.Avatar.Id;
                }


                UsersManager.Edit(model);

                if(model.AvatarUrl != null)
                {
                    if(userses.Avatar != null)
                    {
                        AvatarManager.Delete(oldAvatarId);
                        System.Diagnostics.Debug.WriteLine("et on delete le id : " + oldAvatarId);
                    }
                }

                User user = ((User)Session["userConnected"]);

                if(model.Password != null)
                    user.Password = model.Password;

                if (model.AvatarUrl != null)
                    user.Avatar = AvatarManager.getById(model.IdAvatar);
     
                user.Email = model.Email;

                Session["userConnected"] = user;

                this.ViewBag.Complete = "Information have been updated";

                model.Password = null;
                model.ConfirmPassword = null;
                model.Oldpassword = null;
                ModelState.Clear();
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult searchusermessage(String search)
        {
            SearchMessageUser smu = null;

            if (this.Request.IsAjaxRequest())
             {
                smu = new SearchMessageUser
                {
                    Messages = NotetopicsManager.getListContain(search),
                    Users = UsersManager.getUsersContain(search),
                    MyMessages = NotetopicsManager.AllMessageFromUser(((User)Session["userConnected"]).Id , search)
                };
            }
            else
                return RedirectToAction("notfound");


            return PartialView(smu);
        }

        public ActionResult disconnect()
        {
            Session.Abandon();
            return RedirectToAction("Index");
        }

        public ActionResult Profil(String username)
        {
            if (Session["userConnected"] == null)
                return RedirectToAction("Index");

            User user = UsersManager.getUserByUsername(username);

            if (user == null)
                return RedirectToAction("notfound");

            this.ViewBag.ListPublic = NotetopicsManager.getByUsernamePublic(username);
            return View(user);
        }

        [HttpGet]
        public ActionResult deletemessage(int? mid)
        {
            Notemessage msg = NotetopicsManager.getById(mid);

            if (msg != null)
            {
                List<Noteaddition> additionList = NotetopicsManager.getById(mid).Noteadditions.ToList();
                additionList.ForEach(n => NoteadditionsManager.Delete(n.Id));
                NotetopicsManager.Delete(mid);
            }
            else
                return RedirectToAction("notfound");

            return RedirectToAction("mynotes", new { id = msg.Categorie.Name });
        }

        public ActionResult editmain(int? mid , String categorie)
        {
            if (Session["userConnected"] == null)
                return RedirectToAction("Index");

            Notemessage msg = NotetopicsManager.getById(mid);

            if (msg == null)
                return RedirectToAction("notfound");

            if (!NotesCategoriesManager.CategorieNameExist(categorie))
                return RedirectToAction("notfound");

            this.ViewBag.CategoriesList = DropDownListManager.getNotetopicSelectListItem(categorie);
            return View(msg);
        }

        [HttpPost]
        public ActionResult editmain([Bind(Exclude = "Categorie,ckeditor")]Notemessage msg , String categorie)
        {

            foreach (ModelState modelState in ViewData.ModelState.Values)
            {
                foreach (ModelError error in modelState.Errors)
                {
                    System.Diagnostics.Debug.WriteLine("errorrrrrrrrrrrrrrrrrrrrrrrrrrrrrr : " + error.Exception);
                }
            }

            if (this.ModelState.IsValid)
            {
                System.Diagnostics.Debug.WriteLine("goooooooooooooooooooood : "+msg.Id);
                NotetopicsManager.edit(msg);

                return RedirectToAction("note", new { categorie = categorie, id = msg.Id });
            }

            if (!NotesCategoriesManager.CategorieNameExist(categorie))
                return RedirectToAction("notfound");

            this.ViewBag.CategoriesList = DropDownListManager.getNotetopicSelectListItem(categorie);
            return View(msg);
        }

        public ActionResult edit(int? mid , int? aid)
        {
            if (Session["userConnected"] == null)
                return RedirectToAction("Index");
            
            Noteaddition addition = NoteadditionsManager.get(aid);

            if(addition == null)
                return RedirectToAction("notfound");

            if (addition.Notemessage.UserId != ((User)Session["userConnected"]).Id)
                return RedirectToAction("notfound");

            return View(addition);
        }

        [HttpPost]
        public ActionResult edit(Noteaddition addition)
        {
            Noteaddition add = null;

            if (NoteadditionsManager.get(addition.Id).Notemessage.UserId != ((User)Session["userConnected"]).Id)
                return RedirectToAction("notfound");

            if (this.ModelState.IsValid)
            {
                NoteadditionsManager.Edit(addition);
                add = NoteadditionsManager.get(addition.Id);
                return RedirectToAction("/mynotes/"+add.Notemessage.Categorie.Name+"/note/"+add.NotemessageId);
            }

            return View(addition);
        }

        [HttpGet]
        public ActionResult delete(int? mid ,int? aid , String categorie)
        {
            Notemessage msg = NotetopicsManager.getById(mid);

            if (mid == null || aid == null || categorie == null)
                return RedirectToAction("welcome");

            if(msg.user.Username != ((User)Session["userConnected"]).Username)
                return RedirectToAction("welcome");

            NoteadditionsManager.Delete(aid);

            return RedirectToAction("/mynotes/"+categorie+"/note/"+mid);
        }

        public ActionResult note(String categorie , int? id)
        {
            if (Session["userConnected"] == null)
                return RedirectToAction("Index");

            if (id == null || categorie == null || !NotesCategoriesManager.CategorieNameExist(categorie))
                return RedirectToAction("notfound");

            Notemessage message = NotetopicsManager.getById(id);

            if(message == null)
                return RedirectToAction("notfound");

            if (message.user.Username != ((User)Session["userConnected"]).Username)
                return RedirectToAction("publicnote" , new { id = message.Id });

            if(message.user.Id != ((User)Session["userConnected"]).Id)
                this.ViewBag.NotOwner = true;

            this.ViewBag.Notemessage = message;
            return View();
        }

        [HttpPost]
        public ActionResult note(Noteaddition noteaddition  , int? id)
        {
            if (Session["userConnected"] == null)
                return RedirectToAction("Index");

            if(this.ModelState.IsValid)
            {
                noteaddition.NotemessageId = (int)id; 
                NoteadditionsManager.Add(noteaddition);
                this.TempData["valid"] = true;
            }

            Notemessage message = null;

            if(id != null)
                message = NotetopicsManager.getById(id);
            else
                return RedirectToAction("notfound");

            if (message == null)
                return RedirectToAction("notfound");
            this.ViewBag.Notemessage = message;
            return View(noteaddition);
        }

        public ActionResult newnote(String categorie)
        {
            if (Session["userConnected"] == null)
                return RedirectToAction("Index");

            if (!NotesCategoriesManager.CategorieNameExist(categorie))
                return RedirectToAction("notfound");

            this.ViewBag.CategoriesList = DropDownListManager.getNotetopicSelectListItem(categorie);
            return View();
        }

        [HttpPost]
        public ActionResult newnote([Bind(Exclude = "Categorie")]Notemessage notetopic , String categorie)
        {
            notetopic.UserId = ((User)Session["userConnected"]).Id;

            if (notetopic.ckeditor == null)
                this.ModelState.AddModelError("ckeditor", "You need to fill this input");

            System.Diagnostics.Debug.WriteLine(notetopic.ckeditor);

            foreach (ModelState modelState in ViewData.ModelState.Values)
            {
                foreach (ModelError error in modelState.Errors)
                {
                    System.Diagnostics.Debug.WriteLine("errorrrrrrrrrrrrrrrrrrrrrrrrrrrrrr : " + error.Exception);
                }
            }

            if (this.ModelState.IsValid)
            {
                System.Diagnostics.Debug.WriteLine("on ajouteeeeeeeeeeeeeeeeeeeeeeeeeeeeee");
                int id = NotetopicsManager.Add(notetopic);
                NoteadditionsManager.Add(new Noteaddition
                {
                    NotemessageId = id,
                    Message = notetopic.ckeditor
                });

                return RedirectToAction("note" , new { categorie = categorie , id = id });
            }

            this.ViewBag.CategoriesList = DropDownListManager.getNotetopicSelectListItem("iju");
            return View(notetopic);
        }

        public ActionResult mynotes(String id)
        {
            if (Session["userConnected"] == null)
                return RedirectToAction("Index");

            if (id == null)
            {
                this.ViewBag.Categories = NotesCategoriesManager.getAll();
            }
            else
            {
                if (NotesCategoriesManager.CategorieNameExist(id))
                {
                    if(id != "Subscribes")
                        this.ViewBag.listmessage = NotetopicsManager.getListMessageByUserIdAndCategorie(((User)Session["userConnected"]).Id, NotesCategoriesManager.getId(id));
                    else if (id == "Subscribes")
                    {
                        this.ViewBag.InSub = true;
                        this.ViewBag.listmessage = SubscribeManager.getUserId(((User)Session["userConnected"]).Id);
                        this.ViewBag.InSub = true;
                    }
                    this.ViewBag.CategorieName = id; 
                }
                else
                    return RedirectToAction("Notfound");
            }

            return View();
        }

        public ActionResult notfound()
        {
            if (Session["userConnected"] == null)
                return RedirectToAction("Index");

            return View();
        }

        public ActionResult welcome()
        {
            if (Session["userConnected"] == null)
                return RedirectToAction("Index");

            WelcomeModel wm = new WelcomeModel
            {
                RecentsMessage = NotetopicsManager.getLastNotes(),
                RecentsMessageAddition = NotetopicsManager.getLastNotesWithRecentAddition()
            };

            return View(wm);
        }

        public ActionResult index()
        {
            //si le user est connecter on le redirige a laccueil de quand on est connecte
            if (Session["userConnected"] != null)
            {
                User user = Session["userConnected"] as User;
                return RedirectToAction("Welcome", new { lang = "en"});
            }

            //si il ya eu des erreur de validation par le formulaire de login ou register ou les reprend
            if(this.TempData["viewdata"] != null)
                 ViewData = (ViewDataDictionary)TempData["viewdata"];

            this.ViewBag.DayList = DropDownListManager.getDays();
            this.ViewBag.MonthList = DropDownListManager.getMonths();
            this.ViewBag.YearList = DropDownListManager.getYears();
            return View();
        }

        [HttpPost]
        public ActionResult login(String loginUsername , String loginPassword)
        {
            System.Diagnostics.Debug.WriteLine("usernameeeeeeeeeeeeeeeeeeee : "+loginUsername);
            if (LoginForm.LoginTest(loginUsername , loginPassword))
            {
                if(UsersManager.getUserByUsername(loginUsername).Actif == true)
                    Session["UserConnected"] = UsersManager.getUserByUsername(loginUsername);
                else
                    this.TempData["loginerror"] = "You are banned";
            }
            else
                this.TempData["loginerror"] = "Bad username or password";
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult register(User user)
        {
            //we valid the Birthday
            if (!UsersManager.ValidBirthDay(user))
                this.ModelState.AddModelError("BirthDay", "Invalid Birthday");
            else
                user.BirthDay = new DateTime(user.Year, user.Month, user.Day);

            if (UsersManager.EmailTaken(user.Email))
                this.ModelState.AddModelError("Email", "Email already taken");

            if (UsersManager.UsernameTaken(user.Username))
                this.ModelState.AddModelError("Username", "Username already taken");

            if (this.ModelState.IsValid)
            {
                user.RoleId = 2;
                user.Actif = true;
                UsersManager.Add(user);
                this.ViewBag.DayList = DropDownListManager.getDays();
                this.ViewBag.MonthList = DropDownListManager.getMonths();
                this.ViewBag.YearList = DropDownListManager.getYears();
                return RedirectToAction("Index");
            }

            //si il y a erreur de validation on envoi les erreur dans lindex 
            this.TempData["viewdata"] = ViewData; 

            return RedirectToAction("Index");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}