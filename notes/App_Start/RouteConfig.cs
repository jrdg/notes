﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace notes
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "ajax",
                url: "{lang}/search/{search}",
                defaults: new { controller = "Home", action = "searchusermessage", lang = "en", search = UrlParameter.Optional }
            );

            //new route to try
            routes.MapRoute(
                name: "mynote",
                url: "{lang}/mynotes/{id}",
                defaults: new { controller = "Home", action = "mynotes", lang = "en", id = UrlParameter.Optional}
            );

            //new route to try
            routes.MapRoute(
                name: "publicnote",
                url: "{lang}/note/{id}",
                defaults: new { controller = "Home", action = "publicnote", lang = "en", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "editmain",
                url: "{lang}/mynotes/{categorie}/note/{mid}/edit",
                defaults: new { controller = "Home", action = "editmain", lang = "en", categorie = UrlParameter.Optional, mid = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "edit",
                url: "{lang}/mynotes/{categorie}/note/{mid}/edit/{aid}",
                defaults: new { controller = "Home", action = "edit", lang = "en", categorie = UrlParameter.Optional, mid = UrlParameter.Optional, aid = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "disconnected",
                url: "",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

             routes.MapRoute(
                 name: "newnote",
                 url: "{lang}/mynotes/{categorie}/newnote",
                 defaults: new { controller = "Home", action = "Newnote", lang = "en", categorie = UrlParameter.Optional }
             );

            routes.MapRoute(
                name: "note",
                url: "{lang}/mynotes/{categorie}/note/{id}",
                defaults: new { controller = "Home", action = "note", lang = "en", categorie = UrlParameter.Optional , id = UrlParameter.Optional}
            );

            routes.MapRoute(
                name: "profil",
                url: "{lang}/{action}/{username}",
                defaults: new { controller = "Home", action = "profil", lang = "en", username = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "LoginConnected",
                url: "{lang}/{action}/{id}",
                defaults: new { controller = "Home", action = "LoginIndex", lang = "en" , id = UrlParameter.Optional }
            );

            routes.MapRoute(
                "404-PageNotFound",
                "{*url}",
                new { controller = "Home", action = "notfound" }
            );
        }
    }
}
