﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace notes.Models
{
    public class Avatar
    {
        public int Id { get; set; }
        public String Filename { get; set; }
        public String  Path { get; set; }
    }
}