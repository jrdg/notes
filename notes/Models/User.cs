﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace notes.Models
{
    public class User
    {
        public int Id { get; set; }

        [Required]
        [RegularExpression("^[a-z]+$" , ErrorMessage = "Only accept letter")]
        [Display(Name = "First name")]
        public String FirstName { get; set; }

        [Required]
        [RegularExpression("^[a-z]+$", ErrorMessage = "Only accept letter")]
        [Display(Name = "Last name")]
        public String LastName { get; set; }

        [Required]
        [EmailAddress]
        [Compare("ConfirmEmail",ErrorMessage = "Email and confirmation email need to be the same")]
        [Display(Name = "Email")]
        public String Email { get; set; }

        [Required]
        [EmailAddress]
        [NotMapped]
        [Display(Name = "Confirmation Email")]
        public String ConfirmEmail { get; set; }

        [Required]
        [RegularExpression("^[a-z]+$", ErrorMessage = "Only accept letter")]
        [Display(Name = "Username")]
        public String Username { get; set; }

        public bool?  Actif { get; set; }

        public int? RoleId { get; set; }

        public Role Role { get; set; }

        [Required]
        [Compare("ConfirmPassword", ErrorMessage = "Password and confirmation password need to be the same")]
        [Display(Name = "Password")]
        public String Password { get; set; }

        [Required]
        [Display(Name = "Confirmation password")]
        [NotMapped]
        public String ConfirmPassword { get; set; }

        [NotMapped]
        [Required]
        public int Day { get; set; }

        [NotMapped]
        [Required]
        public int Month { get; set; }

        [NotMapped]
        [Required]
        public int Year { get; set; }

        [Display(Name = "Birthday")]
        public DateTime BirthDay { get; set; }
        public int? AvatarId { get; set; }

        public virtual Avatar Avatar { get; set; }

        public virtual ICollection<Notemessage> Notetopic { get; set; }
    }
}