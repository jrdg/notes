﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace notes.Models
{
    public class SettingFormModel
    {
        public int Id { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Old password")]
        public String Oldpassword { get; set; }

        [DataType(DataType.Password)]
        [Compare("ConfirmPassword" , ErrorMessage = "Password and confirm password need to be the same")]
        public String Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Password confirmation")]
        public String ConfirmPassword { get; set; }

        [EmailAddress]
        [DataType(DataType.EmailAddress)]
        [Required(ErrorMessage = "You need an email")]
        public String Email { get; set; }

        [Display(Name = "Change avatar")]
        public HttpPostedFileBase AvatarUrl { get; set; }

        public int? IdAvatar { get; set; }
    }
}