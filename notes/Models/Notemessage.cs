﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace notes.Models
{
    public class Notemessage
    {
        public int Id { get; set; }

        [Required]
        [RegularExpression("^[a-zA-Z0-9_ ]*$", ErrorMessage = "Only accept letter and number for this field")]
        public String Title { get; set; }

        [Required]
        [RegularExpression("^[a-zA-Z0-9_ ]*$", ErrorMessage = "Only accept letter and number for this field")]
        public String Description { get; set; }

        [Required]
        [Display(Name = "Privacy")]
        public bool Public { get; set; }

        //foreign key property
        public int CategorieId { get; set; }

        public virtual Categorie Categorie { get; set; }

        //foreign key property
        public int UserId { get; set; }

        public virtual User user { get; set; }

        [NotMapped]
        [Display(Name = "Note")]
        [AllowHtml]
        public String ckeditor { get; set; }

        public virtual ICollection<Noteaddition> Noteadditions { get; set; }
    }
}