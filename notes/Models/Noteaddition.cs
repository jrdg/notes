﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace notes.Models
{
    public class Noteaddition
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Message")]
        [AllowHtml]
        public String Message { get; set; }

        public DateTime Dateadd { get; set; }

        //foreign key property
        public int NotemessageId { get; set; }

        public virtual Notemessage Notemessage { get; set; }
    }
}