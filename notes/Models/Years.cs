﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace notes.Models
{
    public class Years
    {
        public int Value { get; set; }
        public String Text { get; set; }
    }
}