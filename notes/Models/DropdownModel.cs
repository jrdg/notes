﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace notes.Models
{
    [NotMapped]
    public class DropdownModel
    {
        public String Value { get; set; }
        public String Text { get; set; }
    }
}