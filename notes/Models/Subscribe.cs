﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using notes.Models;

namespace notes.Models
{
    public class Subscribe
    {
        public int Id { get; set; }
        public int UserId { get; set; } 
        public int NotemessageId { get; set; }
        public Notemessage Notemessage { get; set; }
    }
}