﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace notes.Models
{
    public class Database : DbContext
    {
        public Database():base(nameOrConnectionString: "DBContext") {
 
        }

        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Categorie> Categories { get; set; }
        public virtual DbSet<Notemessage> Notemessage { get; set; }
        public virtual DbSet<Noteaddition> Noteadditions { get; set; }
        public virtual DbSet<Subscribe> Subscribes { get; set; }
        public virtual DbSet<Avatar> Avatars { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
    }
}