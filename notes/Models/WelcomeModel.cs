﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace notes.Models
{
    public class WelcomeModel
    {
        public List<Notemessage> RecentsMessage { get; set; }
        public List<Notemessage> RecentsMessageAddition { get; set; }
    }
}