﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace notes.Models
{
    public class Categorie
    {
        public int Id { get; set; }

        [Required]
        public String Name { get; set; }

        public virtual ICollection<Notemessage> Notetopic { get; set; }
    }
}