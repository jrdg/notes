﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;

namespace notes.Models
{
    [NotMapped]
    public class Month
    {
        public int Value { get; set; }
        public String Text { get; set; }
    }
}