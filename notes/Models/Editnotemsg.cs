﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace notes.Models
{
    public class Editnotemsg
    {
        public int Id { get; set; }

        [Required]
        [RegularExpression("^[a-zA-Z0-9_ ]*$", ErrorMessage = "Only accept letter and number for this field")]
        public String Title { get; set; }

        [Required]
        [RegularExpression("^[a-zA-Z0-9_ ]*$", ErrorMessage = "Only accept letter and number for this field")]
        public String Description { get; set; }

        //foreign key property
        public int CategorieId { get; set; }
    }
}