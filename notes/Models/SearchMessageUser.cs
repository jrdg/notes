﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using notes.Models;
using notes.managers;
using System.ComponentModel.DataAnnotations.Schema;

namespace notes.Models
{
    [NotMapped]
    public class SearchMessageUser
    {
        public List<User> Users { get; set; }
        public List<Notemessage> Messages { get; set; }
        public List<Notemessage> MyMessages { get; set; }
    }
}