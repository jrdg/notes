﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace notes.Models
{
    public class AdminModel
    {
        [Required]
        public String Username { get; set; }

        [Required]
        [Display(Name = "Options")]
        public String BanOrDeban { get; set; }
    }
}